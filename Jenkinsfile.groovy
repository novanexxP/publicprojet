node{
    cleanWs()
    try{
        stage('stage:1'){
            sh "echo 'Hello World !!'"
        }
        stage('stage:2'){
            sh "echo 'Yes, hello jenkins for stage 2  !!'"
        }
    }
    finally{
        cleanWs()
    }
}
pipeline {
    agent any 
    stages {
        stage('clone') { 
            steps {
                sh "rm -rf *"
                sh "git clone https://gitlab.com/novanexxP/publicprojet"
                
            }
        }
        stage('build') { 
            steps {
                sh "cd publicprojet/ && python3 main.py >> preBug"
            }
        }
        stage('run') { 
            steps {
                sh "cd publicprojet/ && python3 main.py >> pythonOut"
            }
        }
        stage('ShowResult') { 
            steps {
                sh "cd publicprojet/ && cat pythonOut"
            }
        }

        stage('Sonarqube env.Variable') {
            steps {
                withSonarQubeEnv('sonarqube'){

                    println env.SONAR_HOST_URL
                    println env.SONAR_CONFIG_NAME
                    println env.SONAR_HOST_URL
                    println env.SONAR_AUTH_TOKEN
               
                }
            }
        }
         
        stage('SonarQube analysis') {
            
            steps {
                    withSonarQubeEnv('sonarqube'){

                       sh '''/var/lib/jenkins/sonar-scanner/bin/sonar-scanner -Dsonar.projectKey=python:JenkinsFile -Dsonar.projectName=JenkinsFile -Dsonar.projectVersion=1.0 -Dsonar.sources=/var/lib/jenkins/workspace/pipelineScript/publicprojet/TESTS -Dsonar.language=py -Dsonar.sourceEncoding=UTF-8 -Dsonar.scm.exclusions.disabled=true'''
                 }
            }
        }
    }
}

